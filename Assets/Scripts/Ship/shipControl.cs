﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class shipControl : MonoBehaviour {
	
	public float X;
	public float Y;
	
	//player
	public GameObject player;
	
	public int toship = 0;
	public int showmenu = 0;
	public int controlboat = 0;
	
	public Vector3 captainpos;
	public Vector3 playerpos;
	public Vector3 difference;
	
	public GameObject cappos;
	
	private float ypos = -2f;  

	private playerMovement playerMove;   
	
	void Start ()
	{
		captainpos = transform.position;

		player = GameObject.FindWithTag ("Player");
		playerMove = player.GetComponent(typeof(playerMovement)) as playerMovement;
	}
	
	// Update is called once per frame
	void Update () 
	{
		
		
		
		if (Input.GetMouseButtonDown (0))
		{
			toship = 0;
		}
		
		if (playerMove.atship == 1)
		{
			this.gameObject.transform.position = player.transform.position;
			
			gameObject.transform.position = new Vector3(player.transform.position.x, 0,player.transform.position.z);
			
			print(gameObject.transform.position.x);
		}
		
		if(Input.GetKey ("return"))
		{
			controlboat = 0;
			captainpos = this.gameObject.transform.position;
		}
		
		
		difference = captainpos - playerpos;
		
		
	}
	
	void OnMouseOver()
	{
		
		
		
		if (Input.GetMouseButtonUp (0)) 
		{
			toship = 1;	
		}
		if (Input.GetMouseButtonDown (0)) 
		{
			toship = 0;	
		}
		
		
	}
	
	void OnTriggerEnter(Collider other) {
		
		if (other.CompareTag ("Player"))
		{
			Debug.Log("I'm at the boat!");
			showmenu = 1;
			toship = 0;
		}
	}
	
	void OnTriggerExit(Collider other) {
		showmenu = 0;
	}
	
	void OnGUI()
	{
		if (showmenu == 1)
		{
			float Xcoord=X+Screen.width/2;
			float Ycoord=Y+Screen.height/2;
			
			GUI.depth = -1;
			if(GUI.Button(new Rect(Xcoord -70, Ycoord -25, 140, 50), "Yes"))
			{
				controlboat = 1;
				showmenu = 0;
				playerpos = player.transform.position;
			}
			
			if(GUI.Button(new Rect(Xcoord-70, Ycoord + 25, 140, 50), "No"))
			{
				controlboat = 0;
				showmenu = 0;
			}
		}
	}
	
}
