﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;
using Pathfinding;

public class enemySoldier : MonoBehaviour {

	//Player
	public Vector3 targetPosition;
	public GameObject player;
	
	
	private Vector3 moveDirection = Vector3.zero;

	//Components
	private Seeker seeker;
	private CharacterController controller;
	private playerControl playerCon;
	private playerHealth playerHea;


	public Path path;
	
	public float speed = 200;
	
	public float turnSpeed = 10.0f;
	
	public float nextWaypointDistance = 3;
	
	private int currentWaypoint = 0;

	//soldier parms
	public bool idlemode= true;
	
	private int soldiervisual = 0;
	public float killAmmount = 1.0f;
	
	//Bullet prefab
	public GameObject AmmoPrefab;
	
	//End of barrel
	public Transform barrelEnd;
	
	public float weaponImpact = 1.0f;
	public float AmmoScale = 0.5f;
	public float AmmoSpeed = 5.0f;
	
	public float firetimer; 
	public float fireTime = 0.5f;
	
	//does the soldier has a visual
	public int playerinsight = 0;
	private int startlooking = 0;
	private int lostplayer = 1;
	public float visualtimer = 0;
	public float outofvisual = 10.0f;
	
	
	public void Start () {
		
		player = GameObject.FindWithTag ("Player");
		seeker = GetComponent<Seeker>();
		controller = GetComponent<CharacterController>();
		
		if(idlemode == false)
		{
			if(lostplayer == 1)
			{
				targetPosition = new Vector3 (Random.Range(-10.0F, 10.0F), 0, Random.Range(-10.0F, 10.0F));
				seeker.StartPath (transform.position,targetPosition, OnPathComplete);
			}
			
		}

		playerCon = player.GetComponent(typeof(playerControl)) as playerControl;
		playerHea = player.GetComponent(typeof(playerHealth)) as playerHealth;
		
		//seeker.StartPath (transform.position,targetPosition, OnPathComplete);
	}
	
	public void OnPathComplete (Path p) {
		Debug.Log ("Yay, we got a path back. Did it have an error? "+p.error);
		if (!p.error) {
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
			
			if(idlemode == false)
			{
				if(lostplayer == 1)
				{
					targetPosition = new Vector3 (Random.Range(-30.0F, 30.0F), 0, Random.Range(-30.0F, 30.0F));
				}
			}
		}
	}
	
	public void FixedUpdate () {
		
		
		if(lostplayer == 0)
		{
			targetPosition = player.transform.position;
		}
		
		if(startlooking == 1)
		{
			// Increment the timer
			visualtimer += Time.deltaTime;
			
			//check how long we can chase the player
			if(visualtimer >= outofvisual) 
			{
				Debug.Log ("Player out of sight");
				lostplayer = 1;
				visualtimer = 0;
				startlooking = 0;
			}
		}

		
		if(playerCon.InWater < 1)
		{
			if(playerinsight == 1)
			{
				lostplayer = 0;
				
				startSeeking();
				
				//rotation of the soldier
				Vector3 clickPoint = player.transform.position;	
				Quaternion targetRotation = Quaternion.LookRotation(clickPoint - transform.position);
				
				targetRotation.x = 0;
				targetRotation.z = 0;
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
				
				
				// Increment the timer
				firetimer += Time.deltaTime;
				
				//Reload time for guard
				if(firetimer >= fireTime) 
				{
					Debug.Log (FireWeapon(1));
					firetimer = 0;
				}
			}
			
		}
		
		
		
		if (path == null) {
			
			//We have no path to move after yet
			return;
		}
		
		if (currentWaypoint >= path.vectorPath.Count) {
			
			seeker.StartPath (transform.position,targetPosition, OnPathComplete);
			
			Debug.Log ("End Of Path Reached");
			return;
		}
		
		//Direction to the next waypoint
		Vector3 dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
		dir *= speed * Time.fixedDeltaTime;
		controller.SimpleMove (dir);
		
		//Check if we are close enough to the next waypoint
		//If we are, proceed to follow the next waypoint
		if (Vector3.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
		
		//killing
		
		var offset = player.transform.position - transform.position;
		
		if(offset.magnitude < 2.0f) 
		{
			Debug.Log ("you are going to have it now!");
			if(playerHea.health >0)
			{
				playerHea.health -= killAmmount;
			}
		}
		
	}
	
	void startSeeking () {
		
		
		Debug.Log ("start seeking");
		if(soldiervisual == 0)
		{
			seeker.StartPath (transform.position,targetPosition, OnPathComplete);
		}
		
		//we have visual
		soldiervisual = 1;
	}
	
	//Fireweapon	
	string FireWeapon (int NumberofBullets)
	{
		
		for(int i=0; i<NumberofBullets; i++)
		{
			Instantiate(AmmoPrefab, barrelEnd.position, barrelEnd.rotation);
		}
		
		return "Fired weapon at player";
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Player"))
		{
			targetPosition = player.transform.position;
			
			playerinsight = 1;
			
			
		}
	}
	
	void OnTriggerExit(Collider other) {
		if (other.CompareTag ("Player"))
		{
			playerinsight = 0;
			startlooking = 1;
		}
	}
	
	
	
} 