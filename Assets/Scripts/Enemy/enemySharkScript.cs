﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class enemySharkScript : MonoBehaviour {

	public GameObject player;
	public int playerinsight = 0;
	
	public float attackSpeed = 6.0f;
	public float returnSpeed = 3.0f;
	
	public float moveSpeed = 6.0f;
	private float moveSpeedFloat = 0.0f;
	private Vector3 moveDirection = Vector3.zero;
	public float killAmmount = 1.0f;
	
	public Vector3 orgposition;
	
	public int MoveSpeed = 10;
	public float turnSpeed = 10.0f;
	
	public float timer = 0;
	public float menutime = 2;

	private playerControl playerCon;
	private playerHealth playerHea;
	private CharacterController controller;
	
	// Use this for initialization
	void Start () {
		
		orgposition = gameObject.transform.position;

		player = GameObject.FindWithTag ("Player");
		playerCon = player.GetComponent(typeof(playerControl)) as playerControl;
		playerHea = player.GetComponent(typeof(playerHealth)) as playerHealth;
		controller = GetComponent<CharacterController>();
		
	}
	
	// Update is called once per frame
	void Update () {
			
		if(playerCon.InWater > 0)
		{
			timer = 0;
			
			if(playerinsight == 1)
			{
				
				if(controller.isGrounded)
				{
					moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
					moveDirection = transform.TransformDirection(moveDirection);
					
					
					moveSpeedFloat = moveSpeed;
					
					
					moveDirection *= moveSpeedFloat;
					
				}
				
				Vector3 clickPoint = player.transform.position;	
				Quaternion targetRotation = Quaternion.LookRotation(clickPoint - transform.position);
				targetRotation.x = 0;
				targetRotation.z = 0;
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
				
				moveSpeed = attackSpeed;
				
				var targetPoint = player.transform.position;
				MoveTowardsTarget (targetPoint);
				
				
				
			}
		}
		
		if(playerCon.InWater == 0)
		{
			timer += Time.deltaTime;
			
			moveSpeed = returnSpeed;
			
			var targetPoint = orgposition;
			MoveTowardsTarget (targetPoint);
		}
		
		if(timer>menutime)
		{
			timer = 0;
			Destroy(gameObject);
		}
		
		
		
		
		
		
	}
	
	
	void MoveTowardsTarget(Vector3 target) {
		var offset = target - transform.position;

		
		if(offset.magnitude > 2.0f) {
			offset = offset.normalized * moveSpeed;
			controller.Move(offset * Time.deltaTime);
		}
		else{
			Debug.Log ("got you!");
			if(playerHea.health >0)
			{
				playerHea.health -= killAmmount;
			}
		}
	}
	
	
	void OnTriggerEnter(Collider other) {
		if (other.CompareTag ("Player"))
		{
			
			playerinsight = 1;
			
		}
	}
	
	void OnTriggerExit(Collider other) {
		if (other.CompareTag ("Player"))
		{
			
			playerinsight = 0;
			
		}
	}
	
	
}

