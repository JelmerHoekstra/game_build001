﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class enemySharkAttack : MonoBehaviour {
	
	public GameObject shark;
	public GameObject player;
	
	public Vector3 playerpos;
	public Vector3 sharkpos;
	public Quaternion sharkrot;
	
	public float timer = 0;
	public float menutime = 5;

	private playerControl playerCon;
	
	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag ("Player");
		playerCon = player.GetComponent(typeof(playerControl)) as playerControl;
	}
	
	// Update is called once per frame
	void Update () {
		

		playerpos = player.transform.position;
		sharkpos = playerpos - new Vector3 (Random.Range(-5.0F, 5.0F), 0, Random.Range(-5.0F, 5.0F));
		
		if(playerCon.InWater == 1)
		{
			timer += Time.deltaTime;
		}
		
		if(timer>menutime)
		{
			timer = 0;
			Sharks ();
		}
		
		if(playerCon.InWater == 0)
		{
			timer = 0;
		}
		
	}
	
	void Sharks () {
		
		Instantiate(shark, sharkpos, sharkrot);
		Debug.Log("there is a shark");
	}
}
