﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class enemyHealth : MonoBehaviour {
	
	public float health = 100f;
	public float value = 150f;
	public GameObject player;

	private playerControl playerCon;
	private playerWealth playerWea;
	
	// Healthinformation
	
	private Transform target;
	public Vector3 offset = Vector3.up;
	public Camera mainCamera;
	private Transform selfTransform;
	
	Vector3 point;
	
	public float corx;
	public float cory;
	
	public float menuon = 0;
	public float turntimeron = 0;
	public float timer = 0;
	public float menutime = 1;
	public float alltime = 3;
	
	public Vector2 size = new Vector2(60,20);
	
	
	// Use this for initialization
	void Start () {
		
		target = gameObject.transform;
		selfTransform = transform;
		mainCamera = Camera.main;
		guiText.text = "this is a cube";
		guiText.material.color = Color.green;

		player = GameObject.FindWithTag ("Player");
		playerCon = player.GetComponent(typeof(playerControl)) as playerControl;
		playerWea = player.GetComponent(typeof(playerWealth)) as playerWealth;
		
	}
	
	// Update is called once per frame
	void Update () {
		


		
		// If health is less than or equal to 0...
		if(health <= 0f)
		{
			playerWea.wealth = playerWea.wealth + value;
			
			Destroy(gameObject);
		}
		
		// Use this for healthinformation
		//selfTransform.position = mainCamera.WorldToViewportPoint(target.position + offset);
		
		point = Camera.main.WorldToScreenPoint(target.position + offset);
		
		corx = selfTransform.position.x;
		cory = selfTransform.position.y;
		
		if(turntimeron == 1)
		{
			timer += Time.deltaTime;
		}
		
		if(timer>menutime)
		{
			menuon = 0;
			turntimeron = 0;
			timer = 0;
		}
		
		if(health<30)
		{
			menuon = 1;
		}
		
	}
	
	void OnMouseOver(){
		
		menuon = 1;
		
	}
	
	void OnMouseExit(){
		timer = 0;
		turntimeron = 1;
	}
	
	
	void OnCollisionEnter(Collision collision) 
	{

		if (collision.gameObject.CompareTag ("KillAmmo"))
		{
			health -= playerCon.weaponImpact;
		}
	}
	
	
	void OnGUI () {
		
		
		if(menuon == 1)
		{
			GUI.Box (new Rect (point.x - size.x/2, Screen.height - point.y, size.x, size.y), health.ToString());
		}
		
		
		
		
	}
	
	
	
}
