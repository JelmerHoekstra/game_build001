﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;
using System; 


public class weaponList : MonoBehaviour {
	public string name;
	public float impact;
	public float speed;
	public float scale;
	
	public weaponList(string newName, float impactRadius, float AmmoSpeed, float AmmoScale)
	{
		name = newName;
		impact = impactRadius;
		speed = AmmoSpeed;
		scale = AmmoScale;
	}
	
}
