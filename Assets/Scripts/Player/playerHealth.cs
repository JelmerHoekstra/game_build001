﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class playerHealth : MonoBehaviour { 
	
	public float health = 100f;                         
	public float resetAfterDeathTime = 0.5f;             
	
	
	private Animator anim;                             
	private playerMovement playerMove;              
	public float timer;                               
	private bool playerDead;                           
	private sceneFadeInOut sceneFade;            
	public GameObject player;
	public GameObject fader;
	
	
	void Awake ()
	{
		
		fader = GameObject.FindWithTag ("Fader");
		sceneFade = GetComponent<sceneFadeInOut>();

		player = GameObject.FindWithTag ("Player");
		playerMove = player.GetComponent(typeof(playerMovement)) as playerMovement;
			
	}
	
	
	void Update ()
	{
		
		if(health <= 0f)
		{
			
			if(!playerDead)
				
				PlayerDying();
			else
			{
				
				PlayerDead();
				LevelReset();
			}
		}
	}
	
	
	void PlayerDying ()
	{
		
		playerDead = true;
		
	}
	
	
	void PlayerDead ()
	{
		
	
		
		playerMove.moveSpeed = 0;
		playerMove.turnSpeed = 0;
		
		
	}
	
	
	void LevelReset ()
	{
		
		timer += Time.deltaTime;
		
		
		if(timer >= resetAfterDeathTime)
			
			sceneFade.EndScene();
	}
	
	
	public void TakeDamage (float amount)
	{
		
		health -= amount;
	}
}
