﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class playerControl : MonoBehaviour {
	
	//Bullet prefab
	public GameObject AmmoPrefab;
	
	//End of barrel
	public Transform barrelEnd;
	
	//Ammount of Ammo
	public int InWater = 0;
	
	//Ammount of Ammo
	public int Ammo = 10;
	
	public const int AMMO_COUNT = 15;
	
	public int AmmoForPistol = AMMO_COUNT;
	public int AmmoForShotgun = AMMO_COUNT;
	public int AmmoForRifle = AMMO_COUNT;
	public int AmmoForCannon = AMMO_COUNT;
	
	//Weaponlist
	List<weaponList> weapons = new List<weaponList>();
	int myWeapon = 0;
	public float weaponImpact = 1.0f;
	public float AmmoScale = 1.0f;
	public float AmmoSpeed = 1.0f;
	
	public string CurrentWeapon;
	
	void Start ()
	{
		weapons.Add (new weaponList("No weapon", 0, 0, 0));
		Debug.Log (SwitchWeapons());
	}
	
	//Fireweapon	
	string FireWeapon (int NumberofBullets)
	{
		
		//check for weapons
		if(weapons.Count <= 1) return "No weapons";
		
		if(myWeapon == 0) return "No weapon selected";
		
		//check for ammo
		if(Ammo <= 0) return "Fired nothing";
		
		for(int i=0; i<NumberofBullets; i++)
		{
			Instantiate(AmmoPrefab, barrelEnd.position, barrelEnd.rotation);
		}
		
		Ammo = Ammo - NumberofBullets;
		
		return "Fired weapon";
	}
	
	//Switch weapons
	string SwitchWeapons ()
	{
		//check for weapons
		if(weapons.Count <= 1) return "No weapons";
		
		Debug.Log (weapons[myWeapon].name);
		
		//Give values to current weapon
		weaponImpact = weapons [myWeapon].impact;
		AmmoScale = weapons [myWeapon].scale;
		AmmoSpeed = weapons [myWeapon].speed;
		CurrentWeapon = weapons [myWeapon].name;
		
		//Debug.Log (weaponImpact);
		
		/*
		foreach(WeaponList weapon in weapons)
		{
			Debug.Log (weapon.name + " " + weapon.weaponNumber + " " + weapon.impact + " " + weapon.speed + " " + weapon.scale);
		}
		*/
		
		return "Weapons found";
	}
	
	
	// Update is called once per frame
	void Update () 
	{
		
		//Left mouse button down for weapon fire
		if(Input.GetMouseButtonDown(0))
		{
			Debug.Log (FireWeapon(1));
		}
		
		
		//press E for weapon switch
		if(Input.GetKeyDown(KeyCode.E))
		{
			if(myWeapon < weapons.Count - 1)
			{
				myWeapon = myWeapon + 1;
			}
			else
			{
				myWeapon = 0;
			}
			
			//Debug.Log (myWeapon);
			Debug.Log (SwitchWeapons());
		}
	}
	
	
	//Collect weapons and ammo
	void OnTriggerEnter(Collider collider)
	{
		if(collider.tag == "Weapons")
		{
			
			weapons.Add( new weaponList(collider.name, collider.gameObject.GetComponent<weaponControl>().AmmoImpact, collider.gameObject.GetComponent<weaponControl>().AmmoSpeed, collider.gameObject.GetComponent<weaponControl>().AmmoScale));
			Destroy(collider.gameObject);
			
			if(weapons.Count > 1)
			{
				myWeapon = myWeapon + 1;
				Debug.Log (SwitchWeapons());
			}
			
			
		}
		
		if(collider.tag == "Ammo")
		{
			Destroy(collider.gameObject);
			Ammo = Ammo + 10;
		}
		
		if(collider.tag == "Water")
		{
			Debug.Log ("swimming");
			InWater = 1;
		}
		
	}
	
	//Collect weapons and ammo
	void OnTriggerExit (Collider collider)
	{
		if(collider.tag == "Water")
		{
			Debug.Log ("swimming");
			InWater = 0;
		}
		
	}
	
	
}
