﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class playerMovement : MonoBehaviour {
	
	public float moveSpeed = 6.0f;
	private float moveSpeedFloat = 0.0f;
	public float jumpSpeed = 8.0f;
	private Vector3 moveDirection = Vector3.zero;
	public float gravity = 20.0f;
	
	public float turnSpeed = 10.0f;
	Plane ground;
	
	public GameObject ship;
	public int MoveSpeed = 10;

	private shipControl shipCon;
	public int atship = 0;

	private CharacterController controller;
	
	// Use this for initialization
	void Start () {

		ship = GameObject.FindWithTag ("Ship");

		shipCon = ship.GetComponent(typeof(shipControl)) as shipControl;
		controller = GetComponent<CharacterController>();
		
	}
	
	// Update is called once per frame
	void Update () 
	{

		if(shipCon.toship == 1)
		{
			var targetPoint = ship.transform.position;
			MoveTowardsTarget (targetPoint);
			
		}
		
		if(shipCon.controlboat == 1)
		{
			if(atship == 0)
			{
				
				var targetPoint = shipCon.captainpos;
				//var targetPoint = new Vector3 (0, 4, 0);
				TakeTarget (targetPoint);
			}
			
		}
		else
		{
			atship = 0;
		}

		
		if(controller.isGrounded)
		{
			moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
			moveDirection = transform.TransformDirection(moveDirection);
			
			if(Input.GetKey(KeyCode.LeftShift))
			{
				moveSpeedFloat = moveSpeed * 2;
			}
			else
			{
				moveSpeedFloat = moveSpeed;
			}
			
			moveDirection *= moveSpeedFloat;
			
			if(Input.GetButton("Jump"))
			{
				moveDirection.y = jumpSpeed;
			}
		}
		
		controller.Move (moveDirection * Time.deltaTime);
		
		//Apply gravity
		moveDirection.y -= gravity * Time.deltaTime;
		
		//Player mouse aim
		ground = new Plane (Vector3.up, transform.position);
		
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		
		
		float dist = 0.0f;
		
		if (ground.Raycast (ray, out dist)) 
		{
			if(shipCon.toship == 1)
			{	
				
			}
			else
			{
				Vector3 clickPoint = new Vector3(ray.GetPoint(dist).x, transform.position.y,ray.GetPoint(dist).z);
				
				Quaternion targetRotation = Quaternion.LookRotation(clickPoint - transform.position);
				
				transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, turnSpeed * Time.deltaTime);
			}
		}
		
		if(shipCon.controlboat == 1)
		{
		}
		
	}
	
	void OnMouseOver()
	{
		
		
		
	}
	
	
	void MoveTowardsTarget(Vector3 target) {
		var offset = target - transform.position;
		
		if(offset.magnitude > .1f) {
			offset = offset.normalized * moveSpeed;
			controller.Move(offset * Time.deltaTime);
		}
	}
	
	void TakeTarget(Vector3 target) {
		var offset = target - transform.position;
		
		if(offset.magnitude > .1f) {
			controller.Move(offset);
			if(offset.magnitude < 2.1f)
			{
				atship = 1;
			}
		}
	}
	
}
