﻿//Script created by Jelmer Hoekstra, contact@jelmerhoekstra.nl
using UnityEngine;
using System.Collections;

public class playerAnimations : MonoBehaviour {
	
	public GameObject player;
	//Swimming or not?
	public int InWater = 0;
	public GameObject ship;

	private playerControl playerCon;
	private playerHealth playerHea;
	private shipControl shipCon;
	
	// Use this for initialization
	void Start () {

		ship = GameObject.FindWithTag ("Ship");
		player = GameObject.FindWithTag ("Player");

		playerCon = player.GetComponent(typeof(playerControl)) as playerControl;
		playerHea = player.GetComponent(typeof(playerHealth)) as playerHealth;
		shipCon = ship.GetComponent(typeof(shipControl)) as shipControl;


		
	}
	
	// Update is called once per frame
	void Update () {


		InWater =  playerCon.InWater;
		
		if(playerHea.health > 0)
		{
			if(shipCon.controlboat == 0)
			{
				if (Input.GetKey ("w") || Input.GetKey ("up")) 
				{
					if(InWater == 0)
					{
						if(Input.GetKey(KeyCode.LeftShift))
						{
							animation.CrossFade("WalkFast_001", 0.2f);
						}
						else 
						{
							animation.CrossFade("Walk_001", 0.2f);
						}
					}
					else
					{
						animation.CrossFade("Swim_001", 0.2f);	
					}
				}
				else
				{
					if(InWater == 0)
					{
						animation.CrossFade("Idle_001", 0.2f);
					}
					else
					{
						animation.CrossFade("SwimIdle_001", 0.2f);
					}
				}
			}
		}
		
		if(shipCon.toship == 1)
		{
			if(InWater == 0)
			{
				animation.CrossFade("Walk_001", 0.2f);
			}
			else
			{
				animation.CrossFade("Swim_001", 0.2f);	
			}
			
		}
		
		
		
		
	}	
}
